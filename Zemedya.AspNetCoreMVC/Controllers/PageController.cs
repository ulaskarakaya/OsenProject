﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Zemedya.Business.Abstract.UnitOfWork;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Entity.Concrete.EtkinlikEntities;
using Zemedya.Entity.Concrete.KariyerFirsatlariEntities;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Model.DTO.EmailDTOs;
using Zemedya.Model.VM.Page;

namespace Zemedya.AspNetCoreMVC.Controllers
{
    public class PageController : Controller
    {
        private IUnitOfWork _unitOfWork;
        public PageController(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }


        [Route("")]
        public IActionResult Media()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            List<Etkinlik> etkinlikler = new List<Etkinlik>();
            List<KariyerFirsati> kariyerFirsatlari = new List<KariyerFirsati>();
            List<Duyuru> duyurular = new List<Duyuru>();
            for (int i = 0; i < 4; i++)
            {
                Etkinlik etkinlik = _unitOfWork.EtkinlikManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                etkinlikler.Add(etkinlik);
                
            }
            for (int i = 0; i < 10; i++)
            {
                KariyerFirsati kariyerFirsati = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                kariyerFirsatlari.Add(kariyerFirsati);

            }
            for (int i = 0; i < 3; i++)
            {
                Duyuru duyuru = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                duyurular.Add(duyuru);

            }
            if (siteBilgisi.SiteTitle == null)
            {
                siteBilgisi.SiteTitle = "Zemedya";
            }
            if (siteBilgisi.GoogleAnalyticsKodu == null)
            {
                siteBilgisi.GoogleAnalyticsKodu = "google";
            }
            AnasayfaVM anasayfaVM = new AnasayfaVM()
            {
                Title = siteBilgisi.SiteTitle.Trim(),
                GoogleDogrulama = siteBilgisi.GoogleDogrulamaKodu,
                Favicon = siteBilgisi.Favicon,
                GoogleAnalytics = siteBilgisi.GoogleAnalyticsKodu.Trim(),
                KariyerFirsatiListesi = kariyerFirsatlari,
                EtkinlikListesi = etkinlikler,
                Kurumsal = _unitOfWork.KurumsalManager.Getir(x => x.Aktif == true),
                DuyuruListesi = duyurular
            };
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            
            return View(anasayfaVM);
        }

        [Route("/en")]
        public IActionResult MediaEN()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            List<Etkinlik> etkinlikler = new List<Etkinlik>();
            List<KariyerFirsati> kariyerFirsatlari = new List<KariyerFirsati>();
            List<Duyuru> duyurular = new List<Duyuru>();
            for (int i = 0; i < 4; i++)
            {
                Etkinlik etkinlik = _unitOfWork.EtkinlikManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                etkinlikler.Add(etkinlik);

            }
            for (int i = 0; i < 10; i++)
            {
                KariyerFirsati kariyerFirsati = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                kariyerFirsatlari.Add(kariyerFirsati);

            }
            for (int i = 0; i < 3; i++)
            {
                Duyuru duyuru = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                duyurular.Add(duyuru);

            }
            if (siteBilgisi.SiteTitleEN == null)
            {
                siteBilgisi.SiteTitleEN = "Zemedya";
            }
            if (siteBilgisi.GoogleAnalyticsKodu == null)
            {
                siteBilgisi.GoogleAnalyticsKodu = "google";
            }
            AnasayfaVM anasayfaVM = new AnasayfaVM()
            {
                Title = siteBilgisi.SiteTitleEN.Trim(),
                GoogleDogrulama = siteBilgisi.GoogleDogrulamaKodu,
                Favicon = siteBilgisi.Favicon,
                GoogleAnalytics = siteBilgisi.GoogleAnalyticsKodu.Trim(),
                KariyerFirsatiListesi = kariyerFirsatlari,
                EtkinlikListesi = etkinlikler,
                Kurumsal = _unitOfWork.KurumsalManager.Getir(x => x.Aktif == true),
                DuyuruListesi = duyurular
            };
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.description = siteBilgisi.DescriptionEN.Trim();
            }
            return View(anasayfaVM);
        }


        [Route("iletisim")]
        public IActionResult Iletisim()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            var iletisim = _unitOfWork.IletisimManager.Getir(x => x.Id == 1);
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            iletisim.Ayarlar = siteBilgisi;
            return View(iletisim);
        }





        public ActionResult EmailGonder([FromForm] EmailDTO emailDTO, IFormFile dosya)
        {
            try
            {
                var ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
                SmtpClient client = new SmtpClient(ayarlar.Server, int.Parse(ayarlar.Port));
                client.Credentials = new NetworkCredential(ayarlar.Adres, ayarlar.Sifre);
                client.EnableSsl = false;
                MailMessage msj = new MailMessage();
                
                    msj.From = new MailAddress(emailDTO.EpostaAdı, emailDTO.Ad + " " + emailDTO.Soyad + " "+ emailDTO.EpostaAdı);
                    msj.To.Add(ayarlar.IletilecekAdres);


                

                    msj.Subject = emailDTO.Konu;
                
                
                msj.Body =  "Mesaj : " +  emailDTO.Mesaj + Environment.NewLine +  "Telefon : " + emailDTO.Telefon + Environment.NewLine + "Ad : " + emailDTO.Ad + Environment.NewLine + "Soyad : " + emailDTO.Soyad + Environment.NewLine + "Email : " + emailDTO.EpostaAdı ;
                if (emailDTO.kurumsal != null)
                {
                    msj.Body = msj.Body + Environment.NewLine + "İletişim Tipi : " + emailDTO.kurumsal;
                }
                if (emailDTO.kurumAdi != null || emailDTO.kurumsalpozisyon != null)
                {
                    msj.Body = msj.Body + Environment.NewLine + "Kurum Adı : " + emailDTO.kurumAdi ?? "" + Environment.NewLine + "Kurumsal Pozisyon : " + emailDTO.kurumsalpozisyon  ?? "";
                }
                if (dosya != null)
                {
                    string FileName = Path.GetFileName(dosya.FileName);
                    msj.Attachments.Add(new Attachment(dosya.OpenReadStream(), FileName));
                }
                client.Send(msj);
                return Json(new
                {
                    success = true,
                    result = 1
                });

            }
            catch (Exception e)
            {

                return Json(new
                {
                    success = true,
                    result = 2
                });
            }
            
        }

      





        public ActionResult SiteBilgileri(string bilgi)
        {
            var ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            return Json(new
            {
                success = true,
                whatsapptel = ayarlar.WhatsappTelefon,
                whatsappdurum = ayarlar.WhatsappDurum,
                whatsappMesaj = ayarlar.WhatsappMesaj,
                canlidestek = ayarlar.CanliDestekKodu,
                canlidestekaktif = ayarlar.CanliDestekAktif
            });
        }

        [Route("hizmetlerimiz/{seo}")]
        [Route("our-services/{seo}")]
        public IActionResult Etkinlik(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var duyuru = _unitOfWork.DuyuruManager.Getir(x => x.SEO == seo);
            if (duyuru == null)
            {
                duyuru = _unitOfWork.DuyuruManager.Getir(x => x.SEOEN == seo);
                ViewBag.en = "True";
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(duyuru);
        }



        [Route("blog")]
        public IActionResult Blog()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var blog = _unitOfWork.EtkinlikManager.HepsiniGetir(x => x.Aktif == true);
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(blog);
        }

        [Route("blogs")]
        public IActionResult BlogEN()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var blog = _unitOfWork.EtkinlikManager.HepsiniGetir(x => x.Aktif == true);
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.description = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(blog);
        }

        [Route("blog/{seo}")]
        public IActionResult BlogDetay(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var blog = _unitOfWork.EtkinlikManager.Getir(x => x.SEO == seo);
            if (blog == null)
            {
                blog = _unitOfWork.EtkinlikManager.Getir(x => x.SEOEN == seo);
                ViewBag.en = "True";
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            return View(blog);
        }

        [Route("kariyer-firsatlari")]
        public IActionResult KariyerFirsatlari()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var kariyer = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif == true);
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            
            return View(kariyer);
        }

        [Route("career-opportunities")]
        public IActionResult KariyerFirsatlariEN()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var kariyer = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif == true);
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.description = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;

            return View(kariyer);
        }


        [Route("kariyer-firsatlari/{seo}")]
        [Route("career-opportunities/{seo}")]
        public IActionResult KariyerFirsatlariDetay(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var kariyer = _unitOfWork.KariyerFirsatiManager.Getir(x => x.SEO == seo);
            if (kariyer == null)
            {
                kariyer = _unitOfWork.KariyerFirsatiManager.Getir(x => x.SEOEN == seo);
                ViewBag.en = "True";
            }
            
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.DescriptionEN != null)
            {
                ViewBag.descriptionEN = siteBilgisi.DescriptionEN.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            List<KariyerFirsati> kariyerFirsatlari = new List<KariyerFirsati>();
            for (int i = 0; i < 5; i++)
            {
                KariyerFirsati kariyerFirsati = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                kariyerFirsatlari.Add(kariyerFirsati);

            }
            kariyer.kariyerFirsatlari = kariyerFirsatlari;
            return View(kariyer);
        }

        [Route("kariyer-basvuru")]
        public IActionResult KariyerBasvuru()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;

            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            List<KariyerFirsati> kariyerFirsatlari = new List<KariyerFirsati>();
            for (int i = 0; i < 5; i++)
            {
                KariyerFirsati kariyerFirsati = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                kariyerFirsatlari.Add(kariyerFirsati);

            }

            return View(kariyerFirsatlari);
        }

        [Route("isveren-basvuru")]
        public IActionResult IsverenBasvuru()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            var duyurular = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif == true);

            return View(duyurular);
        }


        [Route("osen/{seo}")]
        public IActionResult Kurumsal(string seo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            var kurumsal = _unitOfWork.KurumsalManager.Getir(x => x.Aktif == true);
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            if (seo == "about-us")
            {
                ViewBag.en = "True";
            }
            return View(kurumsal);
        }


        [Route("basvuru/{refNo}")]
        public IActionResult Basvuru(string refNo)
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;

            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            List<KariyerFirsati> kariyerFirsatlari = new List<KariyerFirsati>();
            for (int i = 0; i < 5; i++)
            {
                KariyerFirsati kariyerFirsati = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif).ElementAtOrDefault(i);
                kariyerFirsatlari.Add(kariyerFirsati);

            }
            var kariyer = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.ReferansNo == refNo).FirstOrDefault();
            kariyerFirsatlari.Add(kariyer);

            return View(kariyerFirsatlari);
        }

        [Route("isarayan-basvuru")]
        public IActionResult IsarayanBasvuru()
        {
            var siteBilgisi = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            if (siteBilgisi.GoogleDogrulamaKodu != null)
            {
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Trim();
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Replace('"', ' ');
                int koduzunluk = siteBilgisi.GoogleDogrulamaKodu.Length - 6;
                siteBilgisi.GoogleDogrulamaKodu = siteBilgisi.GoogleDogrulamaKodu.Substring(5, koduzunluk);
            }
            ViewBag.googledogrulama = siteBilgisi.GoogleDogrulamaKodu;
            if (siteBilgisi.Description != null)
            {
                ViewBag.description = siteBilgisi.Description.Trim();
            }
            if (siteBilgisi.GoogleAnalyticsKodu != null)
            {
                ViewBag.googleanalytics = siteBilgisi.GoogleAnalyticsKodu.Trim();
            }
            ViewBag.favicon = siteBilgisi.Favicon;
            var kariyerFirsatlari = _unitOfWork.KariyerFirsatiManager.HepsiniGetir(x => x.Aktif == true);

            return View(kariyerFirsatlari);
        }
    }
}
