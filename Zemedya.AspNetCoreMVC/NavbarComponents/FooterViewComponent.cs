﻿using Microsoft.AspNetCore.Mvc;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zemedya.Business.Abstract.UnitOfWork;
using Zemedya.Model.VM.Page;

namespace Zemedya.AspNetCoreMVC.NavbarComponents
{
    public class FooterViewComponent : ViewComponent
    {
        private IUnitOfWork _unitOfWork;
        public FooterViewComponent(IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
        }

        public IViewComponentResult Invoke()
        {
            FooterVM footerVM = new FooterVM();
            footerVM.Iletisim = _unitOfWork.IletisimManager.Getir(x => x.Aktif);
            footerVM.Ayarlar = _unitOfWork.AyarlarManager.Getir(x => x.Id == 1);
            footerVM.Duyurular = _unitOfWork.DuyuruManager.HepsiniGetir(x => x.Aktif);
            return View(footerVM);
        }
    }
}
