﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.AyarEntities;
using Zemedya.Model.DTO.AyarlarDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IAyarlarService
    {
        List<Ayarlar> HepsiniGetir(Expression<Func<Ayarlar, bool>> filter = null);
        AyarlarDTO Getir(Expression<Func<Ayarlar, bool>> filter);
        void Ekle(AyarlarDTO AyarlarDTO);
        void Guncelle(AyarlarDTO AyarlarDTO);
        void KalıcıSil(Expression<Func<Ayarlar, bool>> filter);
    }
}
