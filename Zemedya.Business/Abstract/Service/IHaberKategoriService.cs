﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Model.DTO.HaberDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IHaberKategoriService
    {
        List<HaberKategori> HepsiniGetir(Expression<Func<HaberKategori, bool>> filter = null);
        HaberKategoriDTO Getir(Expression<Func<HaberKategori, bool>> filter);
        void Ekle(HaberKategoriDTO haberKategoriDTO);
        void Guncelle(HaberKategoriDTO haberKategoriDTO);
        void PasifYap(Expression<Func<HaberKategori, bool>> filter);
        void KalıcıSil(Expression<Func<HaberKategori, bool>> filter);
    }
}
