﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.MevcutDillerEntities;
using Zemedya.Model.DTO.MevcutDillerDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IMevcutDillerService
    {
        MevcutDillerDTO Getir(Expression<Func<MevcutDiller, bool>> filter);
        void Guncelle(MevcutDillerDTO mevcutDillerDTO);
    }
}
