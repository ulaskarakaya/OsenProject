﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;
using Zemedya.Model.DTO.MevcutSayfalarDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IMevcutSayfalarService
    {
        MevcutSayfalarDTO Getir(Expression<Func<MevcutSayfalar, bool>> filter);
        void Guncelle(MevcutSayfalarDTO mevcutSayfalarDTO);
    }
}
