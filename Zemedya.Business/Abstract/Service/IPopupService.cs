﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.PopupEntities;
using Zemedya.Model.DTO.PopupDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IPopupService
    {
        List<Popup> HepsiniGetir(Expression<Func<Popup, bool>> filter = null);
        PopupDTO Getir(Expression<Func<Popup, bool>> filter);
        void Ekle(PopupDTO  popupDTO);
        void Guncelle(PopupDTO popupDTO);
        void PasifYap(Expression<Func<Popup, bool>> filter);
        void KalıcıSil(Expression<Func<Popup, bool>> filter);
    }
}
