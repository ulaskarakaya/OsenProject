﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Model.DTO.UrunDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IUrunService
    {
        List<Urun> HepsiniGetir(Expression<Func<Urun, bool>> filter = null);
        UrunDTO Getir(Expression<Func<Urun, bool>> filter);
        void Ekle(UrunDTO UrunDTO);
        void Guncelle(UrunDTO urunDTO);
        void PasifYap(Expression<Func<Urun, bool>> filter);
        void KalıcıSil(Expression<Func<Urun, bool>> filter);
        bool SEOKontrol(string seo);
        bool SEOKontrolEN(string seo);
    }
}
