﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Model.DTO.VideoDTOs;

namespace Zemedya.Business.Abstract.Service
{
    public interface IVideoService
    {
        List<Video> HepsiniGetir(Expression<Func<Video, bool>> filter = null);
        VideoDTO Getir(Expression<Func<Video, bool>> filter);
        void Ekle(VideoDTO VideoDTO);
        void Guncelle(VideoDTO VideoDTO);
        void PasifYap(Expression<Func<Video, bool>> filter);
        void KalıcıSil(Expression<Func<Video, bool>> filter);
    }
}
