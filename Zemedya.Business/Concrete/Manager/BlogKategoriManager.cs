﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Model.DTO.BlogDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class BlogKategoriManager : IBlogKategoriService
    {
        private IMapperService _mapperService;
        private IBlogKategoriDal _blogKategoriDal;
        public BlogKategoriManager(IMapperService mapperService, IBlogKategoriDal blogKategoriDal)
        {
            _mapperService = mapperService;
            _blogKategoriDal = blogKategoriDal;
        }

        public void Ekle(BlogKategoriDTO blogKategoriDTO)
        {
            var entity = _mapperService.Map<BlogKategoriDTO, BlogKategori>(blogKategoriDTO);
            _blogKategoriDal.Ekle(entity);
        }

        public BlogKategoriDTO Getir(Expression<Func<BlogKategori, bool>> filter)
        {
            var entity = _blogKategoriDal.Getir(filter);
            return _mapperService.Map<BlogKategori, BlogKategoriDTO>(entity);
        }


        public void Guncelle(BlogKategoriDTO blogKategoriDTO)
        {
            var entity = _blogKategoriDal.Getir(x => x.Id == blogKategoriDTO.Id);
            var guncelentity = _mapperService.Map<BlogKategoriDTO, BlogKategori>(blogKategoriDTO, entity);
            _blogKategoriDal.Guncelle(guncelentity);
        }

        public List<BlogKategori> HepsiniGetir(Expression<Func<BlogKategori, bool>> filter = null)
        {
            return _blogKategoriDal.HepsiniGetir(filter).OrderBy(x => x.SıralamaNo).ToList();
        }


        public void KalıcıSil(Expression<Func<BlogKategori, bool>> filter)
        {
            _blogKategoriDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<BlogKategori, bool>> filter)
        {
            _blogKategoriDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var entity = _blogKategoriDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public bool SEOKontrolEN(string seo)
        {
            var entity = _blogKategoriDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
