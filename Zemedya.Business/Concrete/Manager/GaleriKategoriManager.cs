﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Model.DTO.GaleriDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class GaleriKategoriManager : IGaleriKategoriService
    {
        private IMapperService _mapperService;
        private IGaleriKategoriDal _galeriKategoriDal;
        public GaleriKategoriManager(IMapperService mapperService, IGaleriKategoriDal galeriKategoriDal)
        {
            _mapperService = mapperService;
            _galeriKategoriDal = galeriKategoriDal;
        }

        public void Ekle(GaleriKategoriDTO GaleriKategoriDTO)
        {
            var entity = _mapperService.Map<GaleriKategoriDTO, GaleriKategori>(GaleriKategoriDTO);
            _galeriKategoriDal.Ekle(entity);
        }

        public GaleriKategoriDTO Getir(Expression<Func<GaleriKategori, bool>> filter)
        {
            var entity = _galeriKategoriDal.Getir(filter);
            return _mapperService.Map<GaleriKategori, GaleriKategoriDTO>(entity);
        }


        public void Guncelle(GaleriKategoriDTO GaleriKategoriDTO)
        {
            var entity = _galeriKategoriDal.Getir(x => x.Id == GaleriKategoriDTO.Id);
            var guncelentity = _mapperService.Map<GaleriKategoriDTO, GaleriKategori>(GaleriKategoriDTO, entity);
            _galeriKategoriDal.Guncelle(guncelentity);
        }

        public List<GaleriKategori> HepsiniGetir(Expression<Func<GaleriKategori, bool>> filter = null)
        {
            return _galeriKategoriDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<GaleriKategori, bool>> filter)
        {
            _galeriKategoriDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<GaleriKategori, bool>> filter)
        {
            _galeriKategoriDal.PasifYap(filter);
        }
        public bool SEOKontrol(string seo)
        {
            var entity = _galeriKategoriDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SEOKontrolEN(string seo)
        {
            var entity = _galeriKategoriDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
