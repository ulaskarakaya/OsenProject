﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.KariyerFirsatlariEntities;
using Zemedya.Model.DTO.KariyerFirsatiDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class KariyerFirsatiManager : IKariyerFirsatiService
    {
        private readonly IKariyerFirsatiDal _kariyerFirsatiDal;
        private IMapperService _mapperService;
        public KariyerFirsatiManager(IKariyerFirsatiDal kariyerFirsatiDal, IMapperService mapperService)
        {
            _kariyerFirsatiDal = kariyerFirsatiDal;
            _mapperService = mapperService;
        }
        public void Ekle(KariyerFirsatiDTO KariyerFirsatiDTO)
        {
            var entity = _mapperService.Map<KariyerFirsatiDTO, KariyerFirsati>(KariyerFirsatiDTO);
            _kariyerFirsatiDal.Ekle(entity);
        }

        public KariyerFirsatiDTO Getir(Expression<Func<KariyerFirsati, bool>> filter)
        {
            var KariyerFirsati = _kariyerFirsatiDal.Getir(filter);
            return _mapperService.Map<KariyerFirsati, KariyerFirsatiDTO>(KariyerFirsati);
        }

        public void Guncelle(KariyerFirsatiDTO KariyerFirsatiDTO)
        {
            var KariyerFirsati = _kariyerFirsatiDal.Getir(x => x.Id == KariyerFirsatiDTO.Id);
            var guncelKariyerFirsati = _mapperService.Map<KariyerFirsatiDTO, KariyerFirsati>(KariyerFirsatiDTO, KariyerFirsati);
            _kariyerFirsatiDal.Guncelle(guncelKariyerFirsati);
        }

        public List<KariyerFirsati> HepsiniGetir(Expression<Func<KariyerFirsati, bool>> filter = null)
        {
            return _kariyerFirsatiDal.HepsiniGetir(filter).OrderByDescending(x => x.Tarih).ToList();

        }

        public void KaliciSil(Expression<Func<KariyerFirsati, bool>> filter)
        {
            _kariyerFirsatiDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<KariyerFirsati, bool>> filter)
        {
            _kariyerFirsatiDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var entity = _kariyerFirsatiDal.Getir(x => x.SEO == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
        public bool SEOKontrolEN(string seo)
        {
            var entity = _kariyerFirsatiDal.Getir(x => x.SEOEN == seo);
            if (entity != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
