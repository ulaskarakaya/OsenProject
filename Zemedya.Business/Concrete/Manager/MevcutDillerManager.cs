﻿using System;
using System.Collections.Generic;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.MevcutDillerEntities;
using Zemedya.Model.DTO.MevcutDillerDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class MevcutDillerManager : IMevcutDillerService
    {
        private IMapperService _mapperService;
        private IMevcutDillerDal _mevcutDillerDal;

        public MevcutDillerManager(IMapperService mapperService, IMevcutDillerDal mevcutDillerDal)
        {
            _mapperService = mapperService;
            _mevcutDillerDal = mevcutDillerDal;
        }

        public MevcutDillerDTO Getir(Expression<Func<MevcutDiller, bool>> filter)
        {
            var entity = _mevcutDillerDal.Getir(filter);
            return _mapperService.Map<MevcutDiller, MevcutDillerDTO>(entity);
        }

        public void Guncelle(MevcutDillerDTO mevcutDillerDTO)
        {
            var entity = _mevcutDillerDal.Getir(x => x.Id == mevcutDillerDTO.Id);
            var guncelentity = _mapperService.Map<MevcutDillerDTO, MevcutDiller>(mevcutDillerDTO, entity);
            _mevcutDillerDal.Guncelle(guncelentity);
        }
    }
}
