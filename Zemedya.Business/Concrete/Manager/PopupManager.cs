﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.PopupEntities;
using Zemedya.Model.DTO.PopupDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class PopupManager : IPopupService
    {
        private IMapperService _mapperService;
        private IPopupDal _popupDal;
        public PopupManager(IMapperService mapperService, IPopupDal popupDal)
        {
            _popupDal = popupDal;
            _mapperService = mapperService;
        }

        public void Ekle(PopupDTO PopupDTO)
        {
            var entity = _mapperService.Map<PopupDTO, Popup>(PopupDTO);
            _popupDal.Ekle(entity);
        }

        public PopupDTO Getir(Expression<Func<Popup, bool>> filter)
        {
            var entity = _popupDal.Getir(filter);
            return _mapperService.Map<Popup, PopupDTO>(entity);
        }


        public void Guncelle(PopupDTO PopupDTO)
        {
            var entity = _popupDal.Getir(x => x.Id == PopupDTO.Id);
            var guncelentity = _mapperService.Map<PopupDTO, Popup>(PopupDTO, entity);
            _popupDal.Guncelle(guncelentity);
        }

        public List<Popup> HepsiniGetir(Expression<Func<Popup, bool>> filter = null)
        {
            return _popupDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Popup, bool>> filter)
        {
            _popupDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Popup, bool>> filter)
        {
            _popupDal.PasifYap(filter);
        }
    }
}
