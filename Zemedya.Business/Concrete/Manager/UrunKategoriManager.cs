﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Model.DTO.UrunDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class UrunKategoriManager : IUrunKategoriService
    {
        private IMapperService _mapperService;
        private IUrunKategoriDal _urunKategoriDal;
        public UrunKategoriManager(IMapperService mapperService, IUrunKategoriDal urunKategoriDal)
        {
            _urunKategoriDal = urunKategoriDal;
            _mapperService = mapperService;
        }

        public void Ekle(UrunKategoriDTO UrunKategoriDTO)
        {
            var entity = _mapperService.Map<UrunKategoriDTO, UrunKategori>(UrunKategoriDTO);
            _urunKategoriDal.Ekle(entity);
        }

        public UrunKategoriDTO Getir(Expression<Func<UrunKategori, bool>> filter)
        {
            var entity = _urunKategoriDal.Getir(filter);
            return _mapperService.Map<UrunKategori, UrunKategoriDTO>(entity);
        }


        public void Guncelle(UrunKategoriDTO UrunKategoriDTO)
        {
            var entity = _urunKategoriDal.Getir(x => x.Id == UrunKategoriDTO.Id);
            var guncelentity = _mapperService.Map<UrunKategoriDTO, UrunKategori>(UrunKategoriDTO, entity);
            _urunKategoriDal.Guncelle(guncelentity);
        }

        public List<UrunKategori> HepsiniGetir(Expression<Func<UrunKategori, bool>> filter = null)
        {
            return _urunKategoriDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<UrunKategori, bool>> filter)
        {
            _urunKategoriDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<UrunKategori, bool>> filter)
        {
            _urunKategoriDal.PasifYap(filter);
        }

        public bool SEOKontrol(string seo)
        {
            var haber = _urunKategoriDal.Getir(x => x.SEO == seo);
            if (haber != null)
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
