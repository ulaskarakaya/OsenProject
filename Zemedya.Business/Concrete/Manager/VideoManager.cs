﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using Zemedya.Business.Abstract.Service;
using Zemedya.DataAccess.Abstract;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Model.DTO.VideoDTOs;

namespace Zemedya.Business.Concrete.Manager
{
    public class VideoManager : IVideoService
    {
        private IMapperService _mapperService;
        private IVideoDal _videoDal;
        public VideoManager(IMapperService mapperService, IVideoDal videoDal)
        {
            _videoDal = videoDal;
            _mapperService = mapperService;
        }

        public void Ekle(VideoDTO VideoDTO)
        {
            var entity = _mapperService.Map<VideoDTO, Video>(VideoDTO);
            _videoDal.Ekle(entity);
        }

        public VideoDTO Getir(Expression<Func<Video, bool>> filter)
        {
            var entity = _videoDal.Getir(filter);
            return _mapperService.Map<Video, VideoDTO>(entity);
        }


        public void Guncelle(VideoDTO VideoDTO)
        {
            var entity = _videoDal.Getir(x => x.Id == VideoDTO.Id);
            var guncelentity = _mapperService.Map<VideoDTO, Video>(VideoDTO, entity);
            _videoDal.Guncelle(guncelentity);
        }

        public List<Video> HepsiniGetir(Expression<Func<Video, bool>> filter = null)
        {
            return _videoDal.HepsiniGetir(filter).ToList();
        }


        public void KalıcıSil(Expression<Func<Video, bool>> filter)
        {
            _videoDal.KaliciSil(filter);
        }

        public void PasifYap(Expression<Func<Video, bool>> filter)
        {
            _videoDal.PasifYap(filter);
        }
    }
}
