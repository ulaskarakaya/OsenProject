﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.GaleriEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IGaleriKategoriDal : IBaseDal<GaleriKategori>
    {
    }
}
