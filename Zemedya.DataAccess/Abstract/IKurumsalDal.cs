﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.KurumsalEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IKurumsalDal : IBaseDal<Kurumsal>
    {
    }
}
