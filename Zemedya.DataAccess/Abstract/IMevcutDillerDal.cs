﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.MevcutDillerEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IMevcutDillerDal : IBaseDal<MevcutDiller>
    {
    }
}
