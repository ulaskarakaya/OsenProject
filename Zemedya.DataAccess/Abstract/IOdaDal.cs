﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.OdaEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IOdaDal: IBaseDal<Oda>
    {
    }
}
