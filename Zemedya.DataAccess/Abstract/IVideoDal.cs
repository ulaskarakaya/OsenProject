﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.VideoEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IVideoDal : IBaseDal<Video>
    {
    }
}
