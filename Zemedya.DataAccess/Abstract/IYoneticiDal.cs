﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.YoneticiEntities;

namespace Zemedya.DataAccess.Abstract
{
    public interface IYoneticiDal : IBaseDal<Yonetici>
    {
    }
}
