﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.BlogMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.DuyuruMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.EtkinlikMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.GaleriMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.HaberMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.IletisimMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.KariyerFirsatiMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.KurumsalMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.OdaMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.PopupMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.ReferansMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.SliderMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.UrunMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.VideoMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.YoneticiMaps;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.SeedData;
using Zemedya.Entity.Concrete.AyarEntities;
using Zemedya.Entity.Concrete.BlogEntities;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Entity.Concrete.EkipEntities;
using Zemedya.Entity.Concrete.EtkinlikEntities;
using Zemedya.Entity.Concrete.GaleriEntities;
using Zemedya.Entity.Concrete.HaberEntities;
using Zemedya.Entity.Concrete.IletisimEntities;
using Zemedya.Entity.Concrete.KariyerFirsatlariEntities;
using Zemedya.Entity.Concrete.KurumsalEntities;
using Zemedya.Entity.Concrete.MevcutDillerEntities;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;
using Zemedya.Entity.Concrete.OdaEntities;
using Zemedya.Entity.Concrete.PopupEntities;
using Zemedya.Entity.Concrete.ReferansEntities;
using Zemedya.Entity.Concrete.SliderEntities;
using Zemedya.Entity.Concrete.UrunEntities;
using Zemedya.Entity.Concrete.VideoEntities;
using Zemedya.Entity.Concrete.YoneticiEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context
{
    public class ProjectContext : DbContext
    {
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            //optionsBuilder.UseLazyLoadingProxies().UseMySql("server=127.0.0.1;port=3306;database=OsenDB;user=root;password=123456");
            optionsBuilder.UseLazyLoadingProxies().UseMySql("server=94.73.149.62;port=3306;database=u7390362_OsenKar;user= u7390362_userBE0;password=NLlj75R5JWqf65K");
        }

        public DbSet<Haber> Haberler { get; set; }
        public DbSet<HaberKategori> HaberKategorileri { get; set; }
        public DbSet<Slider> Sliderlar { get; set; }
        public DbSet<Popup> Popuplar { get; set; }
        public DbSet<Blog> Bloglar { get; set; }
        public DbSet<Urun> Urunler { get; set; }
        public DbSet<UrunKategori> UrunKategorileri { get; set; }
        public DbSet<Yonetici> Yoneticiler { get; set; }
        public DbSet<Iletisim> Iletisimler { get; set; }
        public DbSet<Galeri> Galeriler { get; set; }
        public DbSet<GaleriKategori> GaleriKategorileri { get; set; }
        public DbSet<Kurumsal> KurumsalListesi { get; set; }
        public DbSet<BlogKategori> BlogKategorileri { get; set; }
        public DbSet<MevcutSayfalar> MevcutSayfalar { get; set; }
        public DbSet<MevcutDiller> MevcutDiller { get; set; }
        public DbSet<Ayarlar> Ayarlar { get; set; }
        public DbSet<Duyuru> Duyurular { get; set; }
        public DbSet<Etkinlik> Etkinlikler { get; set; }
        public DbSet<Referans> Referanslar { get; set; }
        public DbSet<Video> Videolar { get; set; }
        public DbSet<Ekip> EkipListesi { get; set; }
        public DbSet<Oda> Odalar { get; set; }
        public DbSet<KariyerFirsati> KariyerFirsatlari { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.ApplyConfiguration(new HaberMap());
            modelBuilder.ApplyConfiguration(new HaberKategoriMap());
            modelBuilder.ApplyConfiguration(new SliderMap());
            modelBuilder.ApplyConfiguration(new PopupMap());
            modelBuilder.ApplyConfiguration(new BlogMap());
            modelBuilder.ApplyConfiguration(new UrunMap());
            modelBuilder.ApplyConfiguration(new UrunKategoriMap());
            modelBuilder.ApplyConfiguration(new YoneticiMap());
            modelBuilder.ApplyConfiguration(new KurumsalMap());
            modelBuilder.ApplyConfiguration(new BlogKategoriMap());
            modelBuilder.ApplyConfiguration(new IletisimMap());
            modelBuilder.ApplyConfiguration(new GaleriMap());
            modelBuilder.ApplyConfiguration(new GaleriKategoriMap());
            modelBuilder.ApplyConfiguration(new DuyuruMap());
            modelBuilder.ApplyConfiguration(new EtkinlikMap());
            modelBuilder.ApplyConfiguration(new ReferansMap());
            modelBuilder.ApplyConfiguration(new VideoMap());
            modelBuilder.ApplyConfiguration(new OdaMap());
            modelBuilder.ApplyConfiguration(new KariyerFirsatiMap());

            modelBuilder.Entity<Haber>().ToTable("haberler");
            modelBuilder.Entity<HaberKategori>().ToTable("haberkategorileri");
            modelBuilder.Entity<Slider>().ToTable("sliderlar");
            modelBuilder.Entity<Popup>().ToTable("popuplar");
            modelBuilder.Entity<Blog>().ToTable("bloglar");
            modelBuilder.Entity<Urun>().ToTable("urunler");
            modelBuilder.Entity<UrunKategori>().ToTable("urunkategorileri");
            modelBuilder.Entity<Yonetici>().ToTable("yoneticiler");
            modelBuilder.Entity<Iletisim>().ToTable("iletisimler");
            modelBuilder.Entity<Galeri>().ToTable("galeriler");
            modelBuilder.Entity<GaleriKategori>().ToTable("galerikategorileri");
            modelBuilder.Entity<Kurumsal>().ToTable("kurumsallistesi");
            modelBuilder.Entity<BlogKategori>().ToTable("blogkategorileri");
            modelBuilder.Entity<MevcutSayfalar>().ToTable("mevcutsayfalar");
            modelBuilder.Entity<MevcutDiller>().ToTable("mevcutdiller");
            modelBuilder.Entity<Ayarlar>().ToTable("ayarlar");
            modelBuilder.Entity<Duyuru>().ToTable("duyurular");
            modelBuilder.Entity<Etkinlik>().ToTable("etkinlikler");
            modelBuilder.Entity<Referans>().ToTable("referanslar");
            modelBuilder.Entity<Video>().ToTable("videolar");
            modelBuilder.Entity<Ekip>().ToTable("ekiplistesi");
            modelBuilder.Entity<KariyerFirsati>().ToTable("kariyerfirsatlari");

            modelBuilder.Seed();
        }
    }
}
