﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.BlogEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfBlogDal : EfBaseDal<Blog, ProjectContext>, IBlogDal
    {
        public EfBlogDal(ProjectContext context) : base(context) { }
    }
}
