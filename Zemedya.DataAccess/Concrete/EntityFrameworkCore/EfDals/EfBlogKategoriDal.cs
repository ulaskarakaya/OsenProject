﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.BlogEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfBlogKategoriDal : EfBaseDal<BlogKategori, ProjectContext>, IBlogKategoriDal
    {
        public EfBlogKategoriDal(ProjectContext context) : base(context) { }
    }
}
