﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.GaleriEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfGaleriKategoriDal : EfBaseDal<GaleriKategori, ProjectContext>, IGaleriKategoriDal
    {
        public EfGaleriKategoriDal(ProjectContext context) : base(context) { }
    }
}
