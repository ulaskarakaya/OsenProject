﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.HaberEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfHaberKategoriDal : EfBaseDal<HaberKategori, ProjectContext>, IHaberKategoriDal
    {
        public EfHaberKategoriDal(ProjectContext context) : base(context)
        {

        }
    }
}
