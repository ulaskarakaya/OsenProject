﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.PopupEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfPopupDal : EfBaseDal<Popup, ProjectContext>, IPopupDal
    {
        public EfPopupDal(ProjectContext context) : base(context)
        {

        }
    }
}
