﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.DataAccess.Abstract;
using Zemedya.DataAccess.Concrete.EntityFrameworkCore.Context;
using Zemedya.Entity.Concrete.UrunEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.EfDals
{
    public class EfUrunKategoriDal : EfBaseDal<UrunKategori, ProjectContext>, IUrunKategoriDal
    {
        public EfUrunKategoriDal(ProjectContext context) : base(context)
        {

        }
    }
}
