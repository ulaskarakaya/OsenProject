﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.HaberEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.HaberMaps
{
    public class HaberKategoriMap : IEntityTypeConfiguration<HaberKategori>
    {
        public void Configure(EntityTypeBuilder<HaberKategori> builder)
        {
            builder.ToTable("HaberKategorileri");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.Isim).HasMaxLength(500);
            builder.Property(x => x.IsimEN).HasMaxLength(500);
        }
    }
}
