﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.IletisimEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.IletisimMaps
{
    public class IletisimMap : IEntityTypeConfiguration<Iletisim>
    {
        public void Configure(EntityTypeBuilder<Iletisim> builder)
        {
            builder.ToTable("Iletisimler");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.Baslik).HasMaxLength(500);
            builder.Property(x => x.BaslikEN).HasMaxLength(500);
        }
    }
}
