﻿using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;
using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.KariyerFirsatlariEntities;

namespace Zemedya.DataAccess.Concrete.EntityFrameworkCore.Mappings.KariyerFirsatiMaps
{
    public class KariyerFirsatiMap : IEntityTypeConfiguration<KariyerFirsati>
    {
        public void Configure(EntityTypeBuilder<KariyerFirsati> builder)
        {
            builder.ToTable("KariyerFirsatlari");
            builder.HasKey(x => x.Id);
            builder.Property(x => x.Id).UseMySqlIdentityColumn();
            builder.Property(x => x.IsAlani).HasMaxLength(100);
            builder.Property(x => x.ReferansNo).HasMaxLength(100);
            builder.Property(x => x.Sehir).HasMaxLength(100);
            builder.Property(x => x.PozisyonTipi).HasMaxLength(100);
            builder.Property(x => x.Pozisyon).HasMaxLength(100);
            builder.Property(x => x.SEO).HasMaxLength(100);
        }
    }
}
