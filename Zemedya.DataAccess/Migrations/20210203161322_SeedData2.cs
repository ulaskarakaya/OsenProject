﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class SeedData2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "BlogKategorileri",
                columns: new[] { "Id", "Aktif", "Gorsel", "GorselEN", "Isim", "IsimEN", "SEO", "SEOEN" },
                values: new object[] { 1, true, null, null, "Genel", null, null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "BlogKategorileri",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
