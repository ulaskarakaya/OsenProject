﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class AnaKategori : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.InsertData(
                table: "UrunKategorileri",
                columns: new[] { "Id", "Aktif", "Gorsel", "GorselEN", "Isim", "IsimEN", "Link", "LinkEN", "SEO", "SEOEN", "UstKategoriId" },
                values: new object[] { 1, true, null, null, "Ana Kategori", null, null, null, null, null, 1 });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "UrunKategorileri",
                keyColumn: "Id",
                keyValue: 1);
        }
    }
}
