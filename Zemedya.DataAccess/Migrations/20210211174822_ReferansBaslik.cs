﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class ReferansBaslik : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Isim",
                table: "Referanslar",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IsimEN",
                table: "Referanslar",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Isim",
                table: "Referanslar");

            migrationBuilder.DropColumn(
                name: "IsimEN",
                table: "Referanslar");
        }
    }
}
