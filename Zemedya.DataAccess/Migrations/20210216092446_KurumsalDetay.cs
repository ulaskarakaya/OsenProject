﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class KurumsalDetay : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "Detay",
                table: "KurumsalListesi",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "DetayEN",
                table: "KurumsalListesi",
                nullable: true);

            migrationBuilder.UpdateData(
                table: "HaberKategorileri",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Gorsel", "GorselEN" },
                values: new object[] { null, null });

            migrationBuilder.UpdateData(
                table: "UrunKategorileri",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Gorsel", "GorselEN" },
                values: new object[] { null, null });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Detay",
                table: "KurumsalListesi");

            migrationBuilder.DropColumn(
                name: "DetayEN",
                table: "KurumsalListesi");

            migrationBuilder.UpdateData(
                table: "HaberKategorileri",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Gorsel", "GorselEN" },
                values: new object[] { "null.png", "null.png" });

            migrationBuilder.UpdateData(
                table: "UrunKategorileri",
                keyColumn: "Id",
                keyValue: 1,
                columns: new[] { "Gorsel", "GorselEN" },
                values: new object[] { "null.png", "null.png" });
        }
    }
}
