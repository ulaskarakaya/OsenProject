﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class DilTablosu2 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MevcutDiller",
                keyColumn: "Id",
                keyValue: 1,
                column: "TR",
                value: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.UpdateData(
                table: "MevcutDiller",
                keyColumn: "Id",
                keyValue: 1,
                column: "TR",
                value: false);
        }
    }
}
