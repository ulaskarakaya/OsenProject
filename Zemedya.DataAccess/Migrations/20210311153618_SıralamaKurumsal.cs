﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class SıralamaKurumsal : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "SıralamaNo",
                table: "KurumsalListesi",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<int>(
                name: "SıralamaNo",
                table: "Etkinlikler",
                nullable: false,
                defaultValue: 0);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "SıralamaNo",
                table: "KurumsalListesi");

            migrationBuilder.DropColumn(
                name: "SıralamaNo",
                table: "Etkinlikler");
        }
    }
}
