﻿using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class OdaEklendi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Odalar",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    Isim = table.Column<string>(nullable: true),
                    IsimEN = table.Column<string>(nullable: true),
                    Link = table.Column<string>(nullable: true),
                    LinkEN = table.Column<string>(nullable: true),
                    Gorsel = table.Column<string>(nullable: true),
                    GorselEN = table.Column<string>(nullable: true),
                    SEO = table.Column<string>(nullable: true),
                    SEOEN = table.Column<string>(nullable: true),
                    Aktif = table.Column<bool>(nullable: false),
                    Detay = table.Column<string>(nullable: true),
                    DetayEN = table.Column<string>(nullable: true),
                    Gorsel_2 = table.Column<string>(nullable: true),
                    Gorsel_2EN = table.Column<string>(nullable: true),
                    SıraNo = table.Column<int>(nullable: false),
                    Gorsel_3 = table.Column<string>(nullable: true),
                    Gorsel_3EN = table.Column<string>(nullable: true),
                    Gorsel_4 = table.Column<string>(nullable: true),
                    Gorsel_4EN = table.Column<string>(nullable: true),
                    Gorsel_5 = table.Column<string>(nullable: true),
                    Gorsel_5EN = table.Column<string>(nullable: true),
                    Gorsel_6 = table.Column<string>(nullable: true),
                    Gorsel_6EN = table.Column<string>(nullable: true),
                    Gorsel_7 = table.Column<string>(nullable: true),
                    Gorsel_7EN = table.Column<string>(nullable: true),
                    Gorsel_8 = table.Column<string>(nullable: true),
                    Gorsel_8EN = table.Column<string>(nullable: true),
                    Wifi = table.Column<bool>(nullable: false),
                    OdaSayisi = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Odalar", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Odalar");
        }
    }
}
