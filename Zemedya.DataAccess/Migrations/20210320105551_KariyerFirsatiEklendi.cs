﻿using System;
using Microsoft.EntityFrameworkCore.Metadata;
using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class KariyerFirsatiEklendi : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "KariyerFirsatlari",
                columns: table => new
                {
                    Id = table.Column<int>(nullable: false)
                        .Annotation("MySql:ValueGenerationStrategy", MySqlValueGenerationStrategy.IdentityColumn),
                    ReferansNo = table.Column<string>(maxLength: 100, nullable: true),
                    Pozisyon = table.Column<string>(maxLength: 100, nullable: true),
                    Sehir = table.Column<string>(maxLength: 100, nullable: true),
                    IsAlani = table.Column<string>(maxLength: 100, nullable: true),
                    PozisyonTipi = table.Column<string>(maxLength: 100, nullable: true),
                    Tarih = table.Column<DateTime>(nullable: false),
                    Detay = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_KariyerFirsatlari", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "KariyerFirsatlari");
        }
    }
}
