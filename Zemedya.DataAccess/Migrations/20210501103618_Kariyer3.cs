﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Zemedya.DataAccess.Migrations
{
    public partial class Kariyer3 : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "Aktif",
                table: "KariyerFirsatlari",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "DetayEN",
                table: "KariyerFirsatlari",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "IsAlaniEN",
                table: "KariyerFirsatlari",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PozisyonEN",
                table: "KariyerFirsatlari",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "PozisyonTipiEN",
                table: "KariyerFirsatlari",
                nullable: true);

            migrationBuilder.AddColumn<string>(
                name: "SEOEN",
                table: "KariyerFirsatlari",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Aktif",
                table: "KariyerFirsatlari");

            migrationBuilder.DropColumn(
                name: "DetayEN",
                table: "KariyerFirsatlari");

            migrationBuilder.DropColumn(
                name: "IsAlaniEN",
                table: "KariyerFirsatlari");

            migrationBuilder.DropColumn(
                name: "PozisyonEN",
                table: "KariyerFirsatlari");

            migrationBuilder.DropColumn(
                name: "PozisyonTipiEN",
                table: "KariyerFirsatlari");

            migrationBuilder.DropColumn(
                name: "SEOEN",
                table: "KariyerFirsatlari");
        }
    }
}
