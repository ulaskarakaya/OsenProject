﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.EtkinlikEntities
{
    public class Etkinlik : BaseEntity, IEntity
    {
        public string Ozet { get; set; }
        public string OzetEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public DateTime Tarih { get; set; }
        public int SıralamaNo { get; set; }

    }
}

