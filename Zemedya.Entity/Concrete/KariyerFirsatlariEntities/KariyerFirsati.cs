﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.KariyerFirsatlariEntities
{
    public class KariyerFirsati : IEntity
    {
        
        public int Id { get; set; }
        public string ReferansNo { get; set; }
        public string Pozisyon { get; set; }
        public string Sehir { get; set; }
        public string IsAlani { get; set; }
        public string PozisyonTipi { get; set; }
        public DateTime Tarih { get; set; }
        public string Detay { get; set; }
        public string SEO { get; set; }
        public bool Aktif { get; set; }
        public string PozisyonEN { get; set; }
        public string IsAlaniEN { get; set; }
        public string PozisyonTipiEN { get; set; }
        public string DetayEN { get; set; }
        public string SEOEN { get; set; }
    }
}
