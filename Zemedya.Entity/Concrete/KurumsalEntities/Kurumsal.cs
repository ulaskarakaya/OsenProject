﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.KurumsalEntities
{
    public class Kurumsal : BaseEntity, IEntity
    {
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public int SıralamaNo { get; set; }
    }
}
