﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.OdaEntities
{
    public class Oda : BaseEntity, IEntity
    {
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public string Gorsel_2 { get; set; }
        public string Gorsel_2EN { get; set; }
        public int SıraNo { get; set; }
        public string Gorsel_3 { get; set; }
        public string Gorsel_3EN { get; set; }
        public string Gorsel_4 { get; set; }
        public string Gorsel_4EN { get; set; }
        public string Gorsel_5 { get; set; }
        public string Gorsel_5EN { get; set; }
        public string Gorsel_6 { get; set; }
        public string Gorsel_6EN { get; set; }
        public string Gorsel_7 { get; set; }
        public string Gorsel_7EN { get; set; }
        public string Gorsel_8 { get; set; }
        public string Gorsel_8EN { get; set; }
        public bool Wifi { get; set; }
        public string OdaSayisi { get; set; }

    }
}
