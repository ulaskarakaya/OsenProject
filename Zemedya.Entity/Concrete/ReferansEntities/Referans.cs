﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.ReferansEntities
{
    public class Referans : IEntity
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public bool Aktif { get; set; }
    }
}
