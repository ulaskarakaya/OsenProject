﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Abstract;

namespace Zemedya.Entity.Concrete.UrunEntities
{
    public class Urun : BaseEntity, IEntity
    {
        public string Kod { get; set; }
        public string Ozet { get; set; }
        public string OzetEN { get; set; }
        public string Detay { get; set; }
        public string DetayEN { get; set; }
        public string Gorsel_2 { get; set; }
        public string Gorsel_2EN { get; set; }
        public int SıraNo { get; set; }
        public string Gorsel_3 { get; set; }
        public string Gorsel_3EN { get; set; }
        public string Gorsel_4 { get; set; }
        public string Gorsel_4EN { get; set; }
        public string Gorsel_5 { get; set; }
        public string Gorsel_5EN { get; set; }
        public string Gorsel_6 { get; set; }
        public string Gorsel_6EN { get; set; }

        public int UrunKategoriId { get; set; }
        public virtual UrunKategori UrunKategori { get; set; }
    }
}
