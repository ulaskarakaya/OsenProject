﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.EkipDTOs
{
    public class EkipDTO
    {
        public int Id { get; set; }
        public string Ad { get; set; }
        public string Soyad { get; set; }
        public string Email { get; set; }
        public string Unvan { get; set; }
        public string Telefon { get; set; }
        public string Departman { get; set; }
        public string Aciklama { get; set; }
        public string Gorsel { get; set; }
        public bool Aktif { get; set; }
    }
}
