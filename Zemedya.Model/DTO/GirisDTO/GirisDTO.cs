﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.GirisDTO
{
    public class GirisDTO
    {
        public string KullaniciAdi { get; set; }
        public string Sifre { get; set; }
        public bool BeniHatirla { get; set; }
    }
}
