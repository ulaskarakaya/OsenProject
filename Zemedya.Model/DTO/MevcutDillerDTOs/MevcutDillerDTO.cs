﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.MevcutDillerDTOs
{
    public class MevcutDillerDTO
    {
        public int Id { get; set; }
        public bool TR { get; set; }
        public bool EN { get; set; }
    }
}
