﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Zemedya.Model.DTO.PopupDTOs
{
    public class PopupDTO
    {
        public int Id { get; set; }
        public string Isim { get; set; }
        public string IsimEN { get; set; }
        public string Link { get; set; }
        public string LinkEN { get; set; }
        public string Gorsel { get; set; }
        public string GorselEN { get; set; }
        public bool Aktif { get; set; }
    }
}
