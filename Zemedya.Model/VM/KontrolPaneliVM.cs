﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.MevcutDillerEntities;
using Zemedya.Entity.Concrete.MevcutSayfalarEntities;
using Zemedya.Model.DTO.MevcutDillerDTOs;
using Zemedya.Model.DTO.MevcutSayfalarDTOs;

namespace Zemedya.Model.VM
{
    public class KontrolPaneliVM
    {
        public MevcutSayfalarDTO MevcutSayfalar { get; set; }
        public MevcutDillerDTO MevcutDiller { get; set; }
    }
}
