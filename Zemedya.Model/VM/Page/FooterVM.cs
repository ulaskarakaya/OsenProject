﻿using System;
using System.Collections.Generic;
using System.Text;
using Zemedya.Entity.Concrete.DuyuruEntities;
using Zemedya.Model.DTO.AyarlarDTOs;
using Zemedya.Model.DTO.IletisimDTOs;

namespace Zemedya.Model.VM.Page
{
    public class FooterVM
    {
        public virtual IletisimDTO Iletisim { get; set; }
        public virtual AyarlarDTO Ayarlar { get; set; }
        public virtual List<Duyuru> Duyurular { get; set; }
    }
}
